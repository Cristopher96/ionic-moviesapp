import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { MovieDetail } from '../interfaces/interfaces';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class DataLocalService {

  movies: MovieDetail[] = [];

  constructor(
    private storage: Storage,
    private toastCtrl: ToastController
  ) {
    this.loadMovie();
  }

  async presentToast(m: string) {
    const toast = await this.toastCtrl.create({
      message: m,
      duration: 1500
    });
    toast.present();
  }

  saveMovie(movie: MovieDetail) {
    let exists = false;
    let msg = '';
    for (const mov of this.movies) {
      if (mov.id === movie.id) {
        exists = true;
        break;
      }
    }

    if (exists) {
      this.movies = this.movies.filter(peli => peli.id !== movie.id);
      msg = 'Removed from favorites';
    } else {
      this.movies.push(movie);
      msg = 'Added to favorites';
    }
    this.presentToast(msg);
    this.storage.set('movies', this.movies);
    return !exists;
  }

  async loadMovie() {
    const movies = await this.storage.get('movies');
    this.movies = movies || [];
    return this.movies;
  }

  async movieExists(id) {
    await this.loadMovie();
    const exists = this.movies.find(peli => peli.id === id);
    return (exists) ? true : false;
  }
}
