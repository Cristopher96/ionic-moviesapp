import { Component, OnInit, Input } from '@angular/core';
import { MoviesService } from '../../services/movies.service';
import { MovieDetail } from 'src/app/interfaces/interfaces';
import { Cast } from '../../interfaces/interfaces';
import { ModalController } from '@ionic/angular';
import { DataLocalService } from '../../services/data-local.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
})
export class DetailComponent implements OnInit {

  @Input() id;
  movie: MovieDetail = {};
  actors: Cast[] = [];
  hide = 150;
  heart = 'heart-empty';

  slideOptActors = {
    slidesPerView: 3.3,
    freeMode: true,
    spaceBetween: -5
  };

  constructor(
    private movieService: MoviesService,
    private modalCtrl: ModalController,
    private dataLocal: DataLocalService
  ) {}

  ngOnInit() {
    this.dataLocal.movieExists(this.id).then(exists =>
     this.heart = (exists) ? 'heart' : 'heart-empty');
    this.movieService.getMovieDetail(this.id).subscribe(resp => {
      this.movie = resp;
    });
    this.movieService.getMovieActors(this.id).subscribe(resp => {
      this.actors = resp.cast;
    });
  }

  back() {
    this.modalCtrl.dismiss();
  }

  favorite() {
    const exists = this.dataLocal.saveMovie(this.movie);
    this.heart = (exists) ? 'heart' : 'heart-empty';
  }
}
