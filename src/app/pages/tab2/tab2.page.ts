import { Component } from '@angular/core';
import { MoviesService } from '../../services/movies.service';
import { Movie } from '../../interfaces/interfaces';
import { ModalController } from '@ionic/angular';
import { DetailComponent } from '../../components/detail/detail.component';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  searchText = '';
  searching = false;
  movies: Movie[] = [];
  ideas: string[] = ['Spiderman', 'Avengers', 'The lord of the rings', 'Your Name'];
  constructor(
    private movieService: MoviesService,
    private modalCtrl: ModalController
  ) {}

  search(e) {
    const value: string = e.detail.value;
    if (value.length === 0) {
      this.searching = false;
      this.movies = [];
      return;
    }
    this.searching = true;
    this.movieService.searchMovie(value).subscribe(resp => {
      // tslint:disable-next-line: no-string-literal
      this.movies = resp['results'];
      this.searching = false;
    });
  }

  async movieDetail(id: string) {
    const modal = await this.modalCtrl.create({
      component: DetailComponent,
      componentProps: {
        id
      }
    });
    modal.present();
  }
}
