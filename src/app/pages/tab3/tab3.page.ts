import { Component, OnInit } from '@angular/core';
import { MovieDetail, Genre } from '../../interfaces/interfaces';
import { DataLocalService } from '../../services/data-local.service';
import { MoviesService } from 'src/app/services/movies.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  movies: MovieDetail[] = [];
  genres: Genre[] = [];
  favoriteByGenre: any[] = [];

  constructor(
    private dataLocal: DataLocalService,
    private moviesSerive: MoviesService
  ) {}

  async ionViewWillEnter() {
    this.movies = await this.dataLocal.loadMovie();
    this.genres = await this.moviesSerive.loadGenres();
    this.moviesByGenre(this.genres, this.movies);
  }

  moviesByGenre(g: Genre[], m: MovieDetail[]) {
    this.favoriteByGenre = [];
    g.forEach(genre => {
      this.favoriteByGenre.push({
        genre: genre.name,
        movies: m.filter(mov => {
          return mov.genres.find(genero => genero.id === genre.id);
        }),
      });
    });
  }
}
